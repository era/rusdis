pub enum Error {}
pub struct Options {}
pub trait KeyValueDB {
    fn get(&self, key: &str) -> String;
    fn set(&self, key: &str, value: String, options: Options) -> Result<String, Error>;
}
