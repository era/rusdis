
use rusdis::rusdis_server::{Rusdis, RusdisServer};
use rusdis::{GetRequest, GetResponse, SetRequest, SetResponse};

use tonic::transport::Server;
use tonic::{Request, Response, Status};
use tracing::instrument;

mod database;

pub mod rusdis {
    tonic::include_proto!("rusdis");
}

#[derive(Debug, Default)]
pub struct RusdisBase {}

#[tonic::async_trait]
impl Rusdis for RusdisBase {
    #[instrument]
    async fn get(&self, _request: Request<GetRequest>) -> Result<Response<GetResponse>, Status> {
        tracing::info!("Starting get work");
        let reply = rusdis::GetResponse {
            value: "abc".into(),
        };
        Ok(Response::new(reply))
    }

    #[instrument]
    async fn set(&self, _request: Request<SetRequest>) -> Result<Response<SetResponse>, Status> {
        let reply = rusdis::SetResponse {};
        Ok(Response::new(reply))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Setups tracing
    let subscriber = tracing_subscriber::FmtSubscriber::new();
    tracing::subscriber::set_global_default(subscriber)?;

    // Setups GRPC server
    let addr = "[::1]:50051".parse()?;
    Server::builder()
        .add_service(RusdisServer::new(RusdisBase::default()))
        .serve(addr)
        .await?;

    Ok(())
}
